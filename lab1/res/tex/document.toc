\babel@toc {russian}{}
\contentsline {section}{\numberline {1.\relax }Постановка задачи}{3}{section.1}%
\contentsline {section}{\numberline {2.\relax }Теория}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1.\relax }Аппроксимация первой производной}{4}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2.\relax }Аппроксимация второй производной}{4}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3.\relax }Метод конечных разностей}{5}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1.\relax }Обыкновенное дифф. уравнение второго порядка}{5}{subsubsection.2.3.1}%
\contentsline {subsubsection}{\numberline {2.3.2.\relax }Параболическое дифф. уравнение}{5}{subsubsection.2.3.2}%
\contentsline {subsubsection}{\numberline {2.3.3.\relax }Уравнение Блэка-Шоулза}{6}{subsubsection.2.3.3}%
\contentsline {section}{\numberline {3.\relax }Алгоритм}{7}{section.3}%
\contentsline {subsubsection}{\numberline {3.0.1.\relax }Явная схема}{7}{subsubsection.3.0.1}%
\contentsline {subsubsection}{\numberline {3.0.2.\relax }Схема Кранка-Нискольсона}{7}{subsubsection.3.0.2}%
\contentsline {subsubsection}{\numberline {3.0.3.\relax }Уравнение Блэка-Шоулза}{8}{subsubsection.3.0.3}%
\contentsline {section}{\numberline {4.\relax }Результаты}{8}{section.4}%
\contentsline {subsection}{\numberline {4.1.\relax }Апроксимация первой производной}{8}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2.\relax }Апроксимация второй производной}{9}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3.\relax }Решение обыкновенного дифф. ур. методом конечных разностей}{10}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4.\relax }Решение параболического дифф. ур. методом конечных разностей}{11}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5.\relax }Решение уравнения Блэка-Шоулза}{13}{subsection.4.5}%
\contentsline {section}{\numberline {5.\relax }Реализация}{14}{section.5}%
\contentsline {section}{\numberline {6.\relax }Приложение}{15}{section.6}%
