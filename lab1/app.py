from lab1.solvers import FiniteDifferences_df, FiniteDifferences_ddf, FiniteDifferencesParabolic, BlackScholesSolver
from matplotlib import pyplot as plt
from math import fabs, sin, cos, log10, log, exp, pi
import numpy as np

funcs = {
    "sin": {
        "u": sin,
        "y": lambda x: -cos(x)
    },
    "x^2": {
        "u": lambda x: x ** 2,
        "y": lambda x: 1 / 3 * x ** 3
    }
}


class Lab1:
    # part_1
    @staticmethod
    def research_error(u, du, ddu, x, name):

        du_left = lambda x, h: (u(x) - u(x - h)) / h
        du_right = lambda x, h: (u(x + h) - u(x)) / h
        du_center = lambda x, h: (u(x + h) - u(x - h)) / 2 / h
        ddu_center = lambda x, h: (u(x + h) - 2 * u(x) + u(x - h)) / h / h

        plt.figure(figsize=(9, 9))
        plt.grid(True)
        # plt.subplot(1, 2, 1)
        pows = []
        accurate_left = []
        accurate_right = []
        accurate_center = []
        accurate_ddf = []
        foundation = 3
        for i in range(abs(int(log(1e-16, foundation)))):
            h = foundation ** -i
            pows.append(i)
            if du(x) - du_left(x, h) != 0:
                accurate_left.append(log10(fabs(du(x) - du_left(x, h))))
            else:
                accurate_left.append(-16)
            if du(x) - du_right(x, h) != 0:
                accurate_right.append(log10(fabs(du(x) - du_right(x, h))))
            else:
                accurate_right.append(-16)
            if du(x) - du_center(x, h) != 0:
                accurate_center.append(log10(fabs(du(x) - du_center(x, h))))
            else:
                accurate_center.append(-16)
            if ddu(x) - ddu_center(x, h) != 0:
                accurate_ddf.append(log10(fabs(ddu(x) - ddu_center(x, h))))
            else:
                accurate_ddf.append(-16)
        # plt.plot(pows, accurate_left, 'r-', label="accurate left(du)")
        # plt.plot(pows, accurate_right, 'g-', label="accurate right(du)")
        # plt.plot(pows, accurate_center, 'b-', label="accurate center(du)")
        plt.plot(pows, accurate_ddf, 'k-', label="accurate left(ddu)")
        plt.xlabel(f"-log[{foundation}](step)")
        plt.ylabel("log[10](accuracy)")
        plt.title(f"u={name}(x)")
        plt.legend()
        plt.show()

    # part_2
    @staticmethod
    def solve_diff_problem():
        h = 1e-3
        a = 0
        b = pi
        ya, yb = 1, 1
        f = lambda x: sin(x) * exp(x) + 1
        ddf = lambda x: 2 * exp(x) * cos(x)
        x, y = FiniteDifferences_ddf(ddf, h, a, b, ya, yb).solve()
        y_exp = np.sin(x) * np.exp(x) + 1
        plt.plot(x, y, 'r-', label="решение с помощью метода кон. разн.")
        plt.plot(x, y_exp, 'b-', label="точное решение")

        error = fabs(y[0] - f(x[0]))
        x_error = x[0]
        for i, x_i in enumerate(x):
            if error < fabs(y[i] - f(x_i)):
                error = fabs(y[i] - f(x_i))
                x_error = x_i
        plt.plot(x_error, f(x_error), 'g*', label="наибольшая ошибка")
        print("log10(error)=" + str(log10(error)))
        plt.title(f"точность {h}")
        plt.grid(True)
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.show()

    @staticmethod
    def research_diff_problem():
        h = 1e-2
        pows = []
        errors = []
        foundation = 10
        N = abs(int(log(1e-7, foundation)))
        for i in range(N):
            h = foundation ** -i
            pows.append(i)
            a = 1
            b = 2
            ya, yb = sin(a) * exp(a), sin(b) * exp(b)
            f = lambda x: sin(x) * exp(x)
            ddf = lambda x: 2 * exp(x) * cos(x)
            x, y = FiniteDifferences_ddf(ddf, h, a, b, ya, yb).solve()

            error = fabs(y[0] - f(x[0]))
            x_error = x[0]
            for i, x_i in enumerate(x):
                if error < fabs(y[i] - f(x_i)):
                    error = fabs(y[i] - f(x_i))
                    x_error = x_i
            errors.append(log10(error)*2)
            print(f"{log10(error)*2}")
        x = np.linspace(0, N, 2)
        k = 2
        y = -2 * x + 1
        y2 = - k * x + 1
        plt.xlabel(f"-log[{foundation}](step)")
        plt.ylabel("log[10](accuracy)")
        plt.plot(pows, errors, "r*", label="полученный порядок ошибки")
        # plt.plot(x, y2, "r-", label=f"апроксимация ошибки O(h^({k}))")
        plt.plot(x, y, "b-", label="ожидаемый порядок ошибки O(h^2)")
        plt.legend()
        plt.show()

    @staticmethod
    def research_parabolic_problem_stability():
        def found_error(u, f, u_0, tao, a, u_a, b, u_b, x_h, t_h):
            solver = FiniteDifferencesParabolic(f, u_0, tao, a, u_a, b, u_b, x_h, t_h)
            u_mtr = solver.solve_crank_nicolson()
            error = 0
            for i, x in enumerate(solver.x):
                for j, t in enumerate(solver.t):
                    error = max(error, fabs(u_mtr[i, j] - u(x, t)))
            return error
        u = lambda x, t: x + sin(t)
        a, b = 0, 1
        tao = 1
        f = lambda x, t: cos(t)
        u_0 = lambda x: x
        u_a = lambda t: sin(t)
        u_b = lambda t: 1 + sin(t)
        x_h, t_h = 0.1, 0.1

        for i in range(5):
            print(found_error(u, f, u_0, tao, a, u_a, b, u_b, x_h, t_h))
            t_h /= 10

    @staticmethod
    def research_parabolic_problem_convergence():
        def found_error(u, f, u_0, tao, a, u_a, b, u_b, x_h, t_h):
            solver = FiniteDifferencesParabolic(f, u_0, tao, a, u_a, b, u_b, x_h, t_h)
            u_mtr = solver.solve_crank_nicolson(0.5)
            error = 0
            for i, x in enumerate(solver.x):
                for j, t in enumerate(solver.t):
                    error = max(error, fabs(u_mtr[i, j] - u(x, t)))
            return error
        u = lambda x, t: x + sin(t)
        a, b = 0, 3
        tao = 1
        f = lambda x, t: cos(t)
        u_0 = lambda x: x
        u_a = lambda t: sin(t)
        u_b = lambda t: 3 + sin(t)
        x_h, t_h = 1, 1
        for i in range(5):
            print(f"x_h={x_h} t_h={t_h}:    ", end="")
            print(found_error(u, f, u_0, tao, a, u_a, b, u_b, x_h, t_h))
            x_h /= 10
            t_h /= 10

    @staticmethod
    def research_black_scholes():
        E = 30
        r = 0.1
        sigma = 0.3
        S_begin = 1
        S_end = 70
        T = 1
        solver = BlackScholesSolver(sigma, r, T, E, S_begin, S_end)
        tau, h = 0.01, 1.
        y = solver.solve(tau, h)
        error = 0
        for i in range(solver.M):
            for j in range(solver.N):
                t = i * tau
                s = S_begin + j * h
                e = fabs(y[i, j] - solver.bsm_call_value(s, t))
                error = max(error, e)
        print(error)

    @staticmethod
    def research_black_scholes_visual():
        E = 30
        r = 0.1
        sigma = 0.3
        S_begin = 1
        S_end = 70
        T = 1
        solver = BlackScholesSolver(sigma, r, T, E, S_begin, S_end)
        tau, h = 0.05, 0.3
        solver.solve(tau, h)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        for i in range(solver.M):
            for j in range(solver.N):
                t = i * tau
                s = S_begin + j * h
                ax.plot(t, s, solver.bsm_call_value(s, t), 'b.')
        ax.legend()
        plt.show()



# x = 0.8
# u = lambda x: sin(x)
# du = lambda x: cos(x)
# ddu = lambda x: -sin(x)
# Lab1.research_error(u, du, ddu, x, "sin")


# Lab1.solve_diff_problem()
Lab1.research_diff_problem()
# Lab1.research_parabolic_problem_stability()
# Lab1.research_parabolic_problem()
# Lab1.research_parabolic_problem_convergence()
# Lab1.research_black_scholes()
# Lab1.research_black_scholes_visual()