import numpy as np
import unittest
from typing import Callable
from scipy import sparse
import scipy.sparse.linalg as sp
from scipy.special import erf
from math import sin, cos, fabs, exp, log, sqrt


class FiniteDifferences_df:
    def __init__(self, u, h, a, b, ya):
        self.a = a
        self.b = b
        self.u = u
        self.h = h
        self.ya = ya

    def solve(self):
        n = int((self.b - self.a) / self.h)
        y = np.zeros(n)
        x = np.zeros(n)
        y[0] = self.ya
        for i in range(1, n):
            x[i] = self.a + self.h * i
            y[i] = self.h * self.u(x[i]) + y[i - 1]
        return x, y


class FiniteDifferences_ddf:
    def __init__(self, u, h, a, b, ya, yb):
        self.a = a
        self.b = b
        self.u = u
        self.h = h
        self.ya = ya
        self.yb = yb

    def solve(self):
        n = int((self.b - self.a) / self.h)
        # y = np.zeros(n + 2)
        c = np.ones(n + 2)
        d = np.full(n + 2, -2)
        e = np.ones(n + 2)
        b = np.zeros(n + 2)
        x = np.zeros(n + 2)

        x[0], c[0], d[0], e[0], b[0] = self.a, 0., 1., 0., self.ya
        x[n+1], c[n+1], d[n+1], e[n+1], b[n+1] = self.b, 0., 1., 0., self.yb
        for i in range(1, n + 1):
            x[i] = self.a + i * self.h
            b[i] = self.h ** 2 * self.u(x[i])
        y = TridiagonalMatrixAlgorithm(c, d, e, b, n + 2).get_answer()
        return x, y


class TridiagonalMatrixAlgorithm:
    def __init__(self, c, d, e, b, n):
        self.n = n
        self.c, self.d, self.e = c, d, e
        self.b = b
        self.alphas, self.bettas = self.__count_koefs()
        self.x = self.__count_x()

    def __count_koefs(self):
        c, d, e = self.c, self.d, self.e
        b = self.b
        alphas = np.zeros(self.n + 1)
        bettas = np.zeros(self.n + 1)
        alphas[1] = -e[0] / d[0]
        bettas[1] = b[0] / d[0]
        for i in range(1, self.n):
            alphas[i + 1] = -e[i] / (d[i] + alphas[i] * c[i])
            bettas[i + 1] = (-c[i] * bettas[i] + b[i]) / (d[i] + alphas[i] * c[i])
        return alphas, bettas

    def __count_x(self):
        n = self.n
        c, d, e, b = self.c, self.d, self.e, self.b
        alphas, bettas = self.alphas, self.bettas
        x = np.zeros(n)
        x[n - 1] = bettas[n]
        for i in reversed(range(n - 1)):
            x[i] = alphas[i + 1] * x[i + 1] + bettas[i + 1]
        return x

    def get_answer(self) -> 'np.ndarray[int]':
        return self.x


class TridiagonalMatrixAlgorithmTest(unittest.TestCase):
    def test_solve1(self):
        c = [0, 1, 1, 1]
        d = [3, 3, 3, 3]
        e = [1, 1, 1, 0]
        b = [4, 5, 5, 4]
        x = TridiagonalMatrixAlgorithm(c, d, e, b, 4).get_answer()
        self.assertTrue(fabs(x[0] - 1) < 1e-2)
        self.assertTrue(fabs(x[1] - 1) < 1e-2)
        self.assertTrue(fabs(x[2] - 1) < 1e-2)
        self.assertTrue(fabs(x[3] - 1) < 1e-2)

    def test_solve2(self):
        c = [0, -3, -5, -6, -5]
        d = [2, 8, 12, 18, 10]
        e = [-1, -1, 2, -4, 0]
        b = [-25, 72, -69, -156, 20]
        x = TridiagonalMatrixAlgorithm(c, d, e, b, 5).get_answer()
        self.assertTrue(fabs(x[4] + 3) < 1e-2)
        self.assertTrue(fabs(x[3] + 10) < 1e-2)
        self.assertTrue(fabs(x[2] + 2) < 1e-2)
        self.assertTrue(fabs(x[1] - 5) < 1e-2)
        self.assertTrue(fabs(x[0] + 10) < 1e-2)


class FiniteDifferencesParabolic:
    def __init__(self,
                 f: Callable[[float, float], float],
                 u_0: Callable[[float], float],
                 tao: float,
                 a: float,
                 u_a: Callable[[float], float],
                 b: float,
                 u_b: Callable[[float], float],
                 h_x: float,
                 h_t: float
                 ):
        self.f = f
        self.u_0 = u_0
        self.tao = tao
        self.a = a
        self.u_a = u_a
        self.b = b
        self.u_b = u_b
        self.h_x = h_x
        self.h_t = h_t
        self.n_x = int((self.b - self.a) / h_x) + 1  # здесь это число всех y
        self.n_t = int(self.tao / self.h_t) + 1
        self.x = np.arange(a, b, h_x)
        self.t = np.arange(0, tao, h_t)

    def solve_obviously(self) -> 'np.ndarray[int]':
        f = self.f
        h_x = self.h_x
        h_t = self.h_t
        n_x = self.n_x
        n_t = self.n_t
        y = np.zeros((n_x, n_t))
        for i in range(n_x):
            x = self.a + i * self.h_x
            y[i, 0] = self.u_0(x)
        for i in range(n_t):
            t = i * self.h_t
            y[0, i] = self.u_a(t)
        for i in range(n_t):
            t = i * self.h_t
            y[n_x - 1, i] = self.u_b(t)
        for t_i in range(1, n_t):
            for x_i in range(1, n_x - 1):
                x = self.a + (x_i - 1) * h_x
                t = (t_i - 1) * h_t
                y[x_i, t_i] = ((y[x_i - 1, t_i - 1] - 2 * y[x_i, t_i - 1] + y[x_i + 1, t_i - 1]) / (h_x ** 2) + f(x,
                                                                                                                  t)) * h_t + \
                              y[x_i, t_i - 1]
        return y

    def solve_crank_nicolson(self, gamma: float = 1) -> 'np.ndarray[int]':
        a = self.a
        b = self.b
        f = self.f
        u_a = self.u_a
        u_b = self.u_b
        u_0 = self.u_0
        x_h = self.h_x
        t_h = self.h_t
        n_x = self.n_x
        n_t = self.n_t
        y = np.array((n_x, n_t))

        size = n_x * n_t

        # vars for build sparse a_mtr
        cols = []
        rows = []
        data = []

        b_mtr = np.zeros(size)
        eq_number = 0
        # side border equations
        for t_i in range(n_t):
            t = t_i * t_h
            # equation for the left border: y[i(t)][0] = u_a[t]
            b_mtr[eq_number] = u_a(t)
            rows.append(eq_number), cols.append(t_i * n_x), data.append(1)
            eq_number = eq_number + 1
            # equation for the right border: y[i(t)][size-1] = u_a[t]
            b_mtr[eq_number] = u_b(t)
            rows.append(eq_number), cols.append(t_i * n_x + n_x - 1), data.append(1)
            eq_number = eq_number + 1
        # down side equations: y[0][i(x)] = u_0[x]
        for x_i in range(1, n_x - 1):
            x = a + x_i * x_h
            b_mtr[eq_number] = u_0(x)
            rows.append(eq_number), cols.append(x_i), data.append(1)
            eq_number = eq_number + 1
        # general equations see pic.1
        for t_i in range(1, n_t):
            for x_i in range(1, n_x - 1):
                t = t_i * t_h
                x = a + x_i * x_h
                b_mtr[eq_number] = f(x, t + t_h / 2)
                k_1 = t_i * n_x + x_i - 1
                k_2 = t_i * n_x + x_i
                k_3 = t_i * n_x + x_i + 1
                k_4 = (t_i - 1) * n_x + x_i - 1
                k_5 = (t_i - 1) * n_x + x_i
                k_6 = (t_i - 1) * n_x + x_i + 1
                rows.append(eq_number), cols.append(k_1), data.append(- gamma / x_h / x_h)
                rows.append(eq_number), cols.append(k_2), data.append(1 / t_h + 2 * gamma / x_h / x_h)
                rows.append(eq_number), cols.append(k_3), data.append(- gamma / x_h / x_h)
                rows.append(eq_number), cols.append(k_4), data.append(- (1 - gamma) / x_h / x_h)
                rows.append(eq_number), cols.append(k_5), data.append(- 1 / t_h + 2 * (1 - gamma) / x_h / x_h)
                rows.append(eq_number), cols.append(k_6), data.append(- (1 - gamma) / x_h / x_h)
                eq_number = eq_number + 1
        a_mtr_s = sparse.coo_matrix((data, (rows, cols)), shape=(size, size))
        u_mtr_1d = sp.spsolve(a_mtr_s, b_mtr)
        u_mtr = np.zeros((n_x, n_t))
        for x_i in range(n_x):
            for t_i in range(n_t):
                u_mtr[x_i, t_i] = u_mtr_1d[t_i * n_x + x_i]
        return u_mtr


class FiniteDifferencesParabolicTest(unittest.TestCase):
    def test_obviously_1(self):
        u = lambda x, t: x + sin(t)
        a, b = 0, 1
        tao = 1
        f = lambda x, t: cos(t)
        u_0 = lambda x: x
        u_a = lambda t: sin(t)
        u_b = lambda t: 1 + sin(t)

        x_h, t_h = 0.01, 0.3

        solver = FiniteDifferencesParabolic(f, u_0, tao, a, u_a, b, u_b, x_h, t_h)
        u_mtr = solver.solve_obviously()

        for t_i in range(0, solver.n_t):
            for x_i in range(0, solver.n_x):
                x = a + x_i * solver.h_x
                t = t_i * solver.h_t
                self.assertTrue(fabs(u(x, t) - u_mtr[x_i, t_i]) < 1)

    def test_obviously_2(self):
        u = lambda x, t: x + t
        a, b = 0, 1
        tao = 1
        f = lambda x, t: 1
        u_0 = lambda x: x
        u_a = lambda t: t
        u_b = lambda t: 1 + t

        x_h, t_h = 0.2, 0.2

        solver = FiniteDifferencesParabolic(f, u_0, tao, a, u_a, b, u_b, x_h, t_h)
        u_mtr = solver.solve_obviously()

        for t_i in range(0, solver.n_t):
            for x_i in range(0, solver.n_x):
                x = a + x_i * solver.h_x
                t = t_i * solver.h_t
                self.assertTrue(fabs(u(x, t) - u_mtr[x_i, t_i]) < x_h)

    def test_crank_nicolson(self):
        u = lambda x, t: x + t
        a, b = 0, 1
        tao = 1
        f = lambda x, t: 1
        u_0 = lambda x: x
        u_a = lambda t: t
        u_b = lambda t: 1 + t

        x_h, t_h = 0.2, 0.2

        solver = FiniteDifferencesParabolic(f, u_0, tao, a, u_a, b, u_b, x_h, t_h)
        u_mtr = solver.solve_crank_nicolson()

        for t_i in range(0, solver.n_t):
            for x_i in range(0, solver.n_x):
                x = a + x_i * solver.h_x
                t = t_i * solver.h_t
                self.assertTrue(fabs(u(x, t) - u_mtr[x_i, t_i]) < x_h)

    def test_crank_nicolson_2(self):
        u = lambda x, t: x + sin(t)
        a, b = 0, 1
        tao = 1
        f = lambda x, t: cos(t)
        u_0 = lambda x: x
        u_a = lambda t: sin(t)
        u_b = lambda t: 1 + sin(t)

        x_h, t_h = 0.2, 0.2

        solver = FiniteDifferencesParabolic(f, u_0, tao, a, u_a, b, u_b, x_h, t_h)
        u_mtr = solver.solve_crank_nicolson()

        for t_i in range(0, solver.n_t):
            for x_i in range(0, solver.n_x):
                x = a + x_i * solver.h_x
                t = t_i * solver.h_t
                self.assertTrue(fabs(u(x, t) - u_mtr[x_i, t_i]) < x_h)


class BlackScholesSolver:

    def __init__(self, sigma, r, T, E, S_start, S_end):
        self.sigma = sigma
        self.r = r
        self.T = T
        self.E = E
        self.S_start, self.S_end = S_start, S_end

    # def exact_solution(self, S, t):
    #     return S * self._N(self._d1(S, t)) - self.E * exp(-self.r * (self.T - t)) * self._N(self._d2(S, t))
    #
    # def _d1(self, S, t):
    #     return (log(S / self.E) + (self.r + self.sigma ** 2 / 2) * (self.T - t)) / (self.sigma * sqrt(self.T - t))
    #
    # def _d2(self, S, t):
    #     return (log(S / self.E) + (self.r - self.sigma ** 2 / 2) * (self.T - t)) / (self.sigma * sqrt(self.T - t))
    #
    # def _N(self, x):
    #     return (1. - erf(-x))

    def bsm_call_value(self, S, t):
        """ Стоимость Европейского опциона call по модели BSM.
        Аналитическая формула.
        Параметры
        ==========
        S0 : float
            текущая цена актива
        K : float
            цена страйка
        T : float
            время до истечения (в частях года)
        r : float
            ставка без риска
        sigma : float
            фактор волатильности
        Возвращает
        =======
        value : float
            Текущая оценка стоимости Европейского call опциона
        """
        from math import log, sqrt, exp
        from scipy import stats
        E = self.E
        T = self.T
        r = self.r
        sigma = self.sigma
        S = float(S)
        d1 = (log(S / E) + (r + 0.5 * sigma ** 2) * T) / (sigma * sqrt(T))
        d2 = (log(S / E) + (r - 0.5 * sigma ** 2) * T) / (sigma * sqrt(T))
        value = (S * stats.norm.cdf(d1, 0.0, 1.0) - E * exp(-r * (T - t)) * stats.norm.cdf(d2, 0.0, 1.0))
        return value

    def solve(self, tau, h):
        sigma, r = self.sigma, self.r
        self.M = M = int(self.T // tau) + 1
        self.N = N = int((self.S_end - self.S_start) // h) + 1
        y = np.zeros((M, N))
        self._set_end_condition(y, M, N)
        self._set_border_conditions(y, tau, M, N)

        for t_ind in reversed(range(M-1)):
            e = np.zeros(N-2)
            d = np.zeros(N-2)
            c = np.zeros(N-2)
            b = np.zeros(N-2)

            b[0] = -1./tau * y[t_ind+1, 1] - (sigma**2 / 2 - r * 1) * y[t_ind, 0]
            e[0] = 0
            d[0] = sigma**2 / 2 - r + 1 / tau
            c[0] = sigma**2 / 2 - r * 2

            for i in range(1, N-3):
                b[i] = -1 / tau * y[t_ind+1, i + 1]
                e[i] = sigma**2 / 2 - r * (i + 1)
                d[i] = sigma**2 / 2 - r + 1 / tau
                c[i] = sigma**2 / 2 - r * (i + 1)
                pass

            b[N-3] = -1. / tau * y[t_ind+1, N-2] - (sigma ** 2 / 2 - r * 1) * y[t_ind, N-1]
            e[N-3] = sigma ** 2 / 2 - r * (N - 3)
            d[N-3] = sigma ** 2 / 2 - r + 1 / tau
            c[N-3] = 0

            x = TridiagonalMatrixAlgorithm(c, d, e, b, N-2).get_answer()
            y[t_ind, 1:N-1] = x
        return y

    def _set_end_condition(self, y, M, N):
        for i in range(N):
            y[M-1, i] = max(0, self.S_end-self.E)

    def _set_border_conditions(self, y, tau, M, N):
        for i in range(M):
            y[i, 0] = 0
            t = i * tau
            y[i, N-1] = self.S_end - self.E * exp(-self.r * (self.T - t))


class BlackScholesSolverTest(unittest.TestCase):
    def test1(self):
        sigma = 1
        r = 1
        T = 1
        E = 1
        S_start = 0
        S_end = 1
        solver = BlackScholesSolver(sigma, r, T, E, S_start, S_end).solve(0.5, 0.5)

